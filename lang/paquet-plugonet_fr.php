<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/plugonet.git

return [

	// P
	'plugonet_description' => 'Ce plugin permet de produire le nouveau fichier paquet.xml à partir du fichier
plugin.xml. Des options de vérification sont disponibles pour nettoyer au préalable
certaines balises du fichier d’origine.

En outre, PlugOnet permet de valider des fichiers paquet.xml écrits manuellement et fournit
une aide sur chaque balise de la nouvelle DTD.',
	'plugonet_slogan' => 'Migrer de la DTD plugin à celle de paquet',
];
