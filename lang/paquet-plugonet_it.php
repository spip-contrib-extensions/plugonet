<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-plugonet?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// P
	'plugonet_description' => 'Questo plugin permette di produrre il nuovo file package.xml dal file plugin.xml. Le opzioni di verifica sono disponibili per la pulizia preliminare di alcuni tag dal file originale.

Inoltre, PlugOnet consente di convalidare i file package.xml scritti manualmente e fornisce aiuto su ogni tag del nuovo DTD.',
	'plugonet_slogan' => 'Migra il plugin DTD al pacchetto',
];
